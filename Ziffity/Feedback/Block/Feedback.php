<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Block;
use Magento\Framework\Controller\ResultFactory;

/**
 * Feedback Frontend BLock
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class Feedback extends \Magento\Framework\View\Element\Template
{
    /**
     * Construct
     *
     * @param \Magento\Framework\View\Element\Template\Context $context
     * @param array $data
     */
    
    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $customerSession;

    /**
     * @var \Magento\Customer\Model\Session
     */
    protected $_moduleManager;
    
    public function __construct(\Magento\Backend\Block\Template\Context $context, \Magento\Customer\Model\Session $customerSession, \Magento\Framework\Module\Manager $moduleManager, array $data = [])
    {
        $this->customerSession = $customerSession;
        $this->_moduleManager = $moduleManager;
        parent::__construct($context, $data);
    }

    /**
     * Get form action URL for POST booking request
     *
     * @return string URL
     */
    public function getActionUrl()
    {
        return '/feedback';
    }

    /**
     * Get form action URL for POST Feedback request
     *
     * @return string
     */
    public function getCustomerData()
    {
        $data = $this->customerSession->get();
        $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
        $customerFactory = $objectManager->get('\Magento\Customer\Model\CustomerFactory')->create();
        $customer = array();
        if(!empty($data['customer_id']))
        {
            $customerId = $data['customer_id'];
            $customer = $customerFactory->load($customerId);
        }
        return $customer;
    }

    /**
     * Whether a module is enabled in the configuration or not
     *
     * @param string $moduleName Fully-qualified module name
     * @return boolean
     */
    public function isModuleEnabled($moduleName)
    {
        return $this->_moduleManager->isEnabled($moduleName);
    }
    
    /**
     * Whether a module output is permitted by the configuration or not
     *
     * @param string $moduleName Fully-qualified module name
     * @return boolean
     */
    public function isOutputEnabled($moduleName)
    {
        return $this->_moduleManager->isOutputEnabled($moduleName);
    }
}