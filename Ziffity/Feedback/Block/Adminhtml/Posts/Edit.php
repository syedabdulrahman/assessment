<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Block\Adminhtml\Posts;
use Magento\Backend\Block\Widget\Form\Container;
use Magento\Backend\Block\Widget\Context;

/**
 * Feedback Admin Grid Edit Block
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class Edit extends Container
{
  /**
    * @param Context $context
    * @param array $data
  */
  public function __construct(Context $context,array $data = [])
  {
    parent::__construct($context, $data);
  }

  /**
   * Class constructor
   *
   * @return void
  */
  protected function _construct()
  {
    $this->_objectId = 'id';
    $this->_controller = 'adminhtml_posts';
    $this->_blockGroup = 'Ziffity_Feedback';
    parent::_construct();
    $this->buttonList->update('save', 'label', __('Save'));
    $this->buttonList->update('delete', 'label', __('Delete'));
  }
}
