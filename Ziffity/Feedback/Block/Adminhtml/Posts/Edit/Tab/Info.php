<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

namespace Ziffity\Feedback\Block\Adminhtml\Posts\Edit\Tab;

use Magento\Backend\Block\Widget\Form\Generic;
use Magento\Backend\Block\Widget\Tab\TabInterface;
use Magento\Backend\Block\Template\Context;
use Magento\Framework\Registry;
use Magento\Framework\Data\FormFactory;

/**
 * Feedback Admin Grid Edit info Block
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class Info extends Generic implements TabInterface
{
  /**
    * @param Context $context
    * @param Registry $registry
    * @param FormFactory $formFactory
    * @param Config $wysiwygConfig
    * @param Status $newsStatus
    * @param array $data
    */
  public function __construct(Context $context, Registry $registry, FormFactory $formFactory, array $data = [])
  {
    parent::__construct($context, $registry, $formFactory, $data);
  }

  /**
    * Prepare form fields
    *
    * @return \Magento\Backend\Block\Widget\Form
    */
  protected function _prepareForm()
  {
    /** @var $model \Ziffity\Feedback\Model\Post */
    $model = $this->_coreRegistry->registry('feedback_post');
    $objectManager = \Magento\Framework\App\ObjectManager::getInstance();
    $postFactory = $objectManager->get('\Ziffity\Feedback\Model\PostFactory')->create();
    $feedbackId = $this->getRequest()->getParam('id');
    $data = $postFactory->load($feedbackId);

    /** @var \Magento\Framework\Data\Form $form */
    $form = $this->_formFactory->create();
    $form->setFieldNameSuffix('feedback');

    $fieldset = $form->addFieldset('base_fieldset',['legend' => __('Info')]);

    if($data->getId()) 
    {
      $fieldset->addField(
        'id',
        'hidden',
        ['name' => 'id']
      );
    }
    $fieldset->addField(
        'customer_fname',
        'text',
        [
          'name'     => 'customer_fname',
          'label'    => __('First Name'),
          'required' => true
        ]
    );
    $fieldset->addField(
      'customer_lname',
      'text',
      [
        'name'     => 'customer_lname',
        'label'    => __('Last Name'),
        'required' => true
      ]
    );
    $fieldset->addField(
      'customer_email',
      'text',
      [
        'name'     => 'customer_email',
        'label'    => __('email'),
        'required' => true
      ]
    );
    $fieldset->addField(
      'status',
      'select',
      [
        'name'    => 'status',
        'label'   => __('Status'),
        'options' => ['1' => __('Approved'), '0' => __('Declined')],
      ]
    );
    $fieldset->addField(
      'customer_comment',
      'textarea',
      [
        'name'     => 'customer_comment',
        'label'    => __('Comment'),
        'required' => true,
        'style'    => 'height: 15em; width: 30em;'
      ]
    );
    $form->setValues($data);
    $this->setForm($form);
    return parent::_prepareForm();
  }

  /**
   * Prepare label for tab
   *
   * @return string
   */
  public function getTabLabel()
  {
    return __('News Info');
  }
 
  /**
   * Prepare title for tab
   *
   * @return string
   */
  public function getTabTitle()
  {
    return __('News Info');
  }
 
  /**
   * {@inheritdoc}
   */
  public function canShowTab()
  {
    return true;
  }

  /**
   * {@inheritdoc}
   */
  public function isHidden()
  {
    return false;
  }
}