<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Block\Adminhtml;

/**
 * Feedback Admin Grid listing BLock
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */

class Posts extends \Magento\Backend\Block\Widget\Grid\Container
{
	/**
  	* Construct
    *
    */
	protected function _construct()
	{
		$this->_controller = 'adminhtml_posts';
		$this->_blockGroup = 'Ziffity_Feedback';
		$this->_headerText = __('Posts');
		parent::_construct();
		$this->removeButton('add');		
	}
}