<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */  
namespace Ziffity\Feedback\Controller\Adminhtml\Posts;
  
/**
 * Feedback Admin grid listing controller
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
use Ziffity\Feedback\Controller\Adminhtml\Posts;
class Grid extends Posts
{
   /**
     * @return void
     */
   public function execute()
   {
      return $this->resultPageFactory->create();
   }
}