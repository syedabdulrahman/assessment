<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Controller\Adminhtml\Posts;
use Ziffity\Feedback\Controller\Adminhtml\Posts;

/**
 * Feedback admin grid Delete controller
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */

class Delete extends Posts
{
   /**
    * @param int $feedId
   * @return void
   */
   public function execute()
   {
      $feedId = (int) $this->getRequest()->getParam('id');
      if ($feedId)
      {
         /** @var $postModel Ziffity\Feedback\Model\Post */
         $postModel = $this->postFactory->create();
         $postModel->load($feedId);

         // Check this feedback exists or not
         if (!$postModel->getId()) 
         {
            $this->messageManager->addError(__('This feedback no longer exists.'));
         } 
         else 
         {
            try 
            {
               // Delete feedback
               $postModel->delete();
               $this->messageManager->addSuccess(__('The feedback has been deleted.'));

               // Redirect to grid page
               $this->_redirect('*/*/');
               return;
            }
            catch (\Exception $e) 
            {
               $this->messageManager->addError($e->getMessage());
               $this->_redirect('*/*/edit', ['id' => $postModel->getId()]);
            }
         }
      }
   }
}