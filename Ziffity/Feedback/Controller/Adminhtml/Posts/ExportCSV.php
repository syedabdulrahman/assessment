<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Controller\Adminhtml\Posts; 
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\App\Response\Http\FileFactory;
use Magento\Framework\File\Csv;
use Magento\Framework\App\Filesystem\DirectoryList;
use Ziffity\Feedback\Model\Post;
use Ziffity\Feedback\Model\PostFactory;
use Magento\Framework\Controller\ResultFactory;
 
/**
 * Feedback admin grid Edit controller
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class ExportCSV extends Action
{
  /**
   * @var \Magento\Framework\App\Response\Http\FileFactory
   */
  protected $fileFactory;
  /**
   * @var \Magento\Framework\File\Csv
   */
  protected $csvProcessor;
  /**
   * @var \Magento\Framework\App\Filesystem\DirectoryList
   */
  protected $directoryList;
  /**
   * @var \Ziffity\Feedback\Model\PostFactory
   */
  protected $postFactory;
  /**
   * @var \Magento\Framework\Controller\ResultFactory
   */
  protected $resultRedirect;

  /**
   * @param \Magento\Backend\App\Action\Context $context
   * @param \Magento\Framework\App\Response\Http\FileFactory $fileFactory
   * @param \Magento\Framework\File\Csv $csvProcessor
   * @param \Magento\Framework\App\Filesystem\DirectoryList $directoryList
   * @param \Ziffity\Feedback\Model\PostFactory $postFactory
   * @param \Magento\Framework\Controller\ResultFactory $resultRedirect
   */

  public function __construct(Context $context, FileFactory $fileFactory, Csv $csvProcessor, DirectoryList $directoryList, PostFactory $postFactory, array $data = array())
  {
    $this->fileFactory = $fileFactory;
    $this->csvProcessor = $csvProcessor;
    $this->directoryList = $directoryList;
    $this->PostFactory = $postFactory;
    parent::__construct($context, $data);
  }

  /**
   * Feedback Export CSV action
   *
   * @return array $resultPage
   */
  public function execute()
  {    
    $fileName = 'csv_filename.csv';
    $filePath = $this->directoryList->getPath(\Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR) . "/" . $fileName;
    
    $model = $this->PostFactory->create()->getCollection();
    $personalData = $this->getPresonalData($model);
    if(count($personalData)>0){
      $this->csvProcessor
        ->setDelimiter(';')
        ->setEnclosure('"')
        ->saveData(
          $filePath,
          $personalData
        );
      return $this->fileFactory->create(
        $fileName,
        [
          'type' => "filename",
          'value' => $fileName,
          'rm' => true,
        ],
        \Magento\Framework\App\Filesystem\DirectoryList::VAR_DIR,'application/octet-stream'
      );    
    }else{
      $this->messageManager->addSuccess(__('Sorry, There is no record to download.'));
      $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
      $resultRedirect->setUrl($this->_redirect->getRefererUrl());
      return $resultRedirect;
    }
  }

  /**
   * Feedback admin html get the modle values.
   *
   * @return bool
   */

  protected function getPresonalData($PostFactory)
  {
    $customerData = $PostFactory->getData();
    return $customerData;
  }

  /**
   * Feedback access rights checking
   *
   * @return bool
   */
  protected function _isAllowed()
  {
    return true;
  }
}