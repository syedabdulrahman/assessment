<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Controller\Adminhtml\Posts;
use Ziffity\Feedback\Controller\Adminhtml\Posts;

/**
 * Feedback admin grid edit save controller
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class Save extends Posts
{
   /**
     * @param int $feedId
     * @return void
     */
   public function execute()
   {
      $isPost = $this->getRequest()->getPost();
      if ($isPost) {
         $postModel = $this->postFactory->create();
         $feedId = $this->getRequest()->getParam('id');
         if ($feedId) {
            $postModel->load($feedId);
         }
         $formData = $this->getRequest()->getParam('feedback');
         $postModel->setData($formData);         
         try {
            // Save feedback
            $postModel->save();
            // Display success message
            $this->messageManager->addSuccess(__('The feedback has been saved.'));
            // Check if 'Save and Continue'
            if ($this->getRequest()->getParam('back')) {
               $this->_redirect('*/*/edit', ['id' => $postModel->getId(), '_current' => true]);
               return;
            }
            // Go to grid page
            $this->_redirect('*/*/');
            return;
         } catch (\Exception $e) {
            $this->messageManager->addError($e->getMessage());
         }
         $this->_getSession()->setFormData($formData);
         $this->_redirect('*/*/edit', ['id' => $feedId]);
      }
   }
}