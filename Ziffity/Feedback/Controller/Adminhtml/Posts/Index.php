<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Controller\Adminhtml\Posts;
use Ziffity\Feedback\Controller\Adminhtml\Posts;
use Ziffity\Feedback\Model\PostFactory;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;

/**
 * Feedback admin grid index controller
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class Index extends Posts
{
  /**
    * @param \Magento\Backend\App\Action\Context $context
    * @param Magento\Framework\Registry $coreRegistry
    * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
    * @param \Ziffity\Feedback\Model\PostFactory $postFactory
    */
  public function __construct(Context $context, Registry $coreRegistry, PageFactory $resultPageFactory, PostFactory $postFactory)
  {
    parent::__construct($context, $coreRegistry, $resultPageFactory, $postFactory);
  }
  
  /**
   * Feedback admin gird edit action
   *
   * @return array $resultPage
   */
  public function execute()
  {
    if ($this->getRequest()->getQuery('ajax')) {
      $this->_forward('grid');
      return;
    }

    /** @var \Magento\Backend\Model\View\Result\Page $resultPage */
    $resultPage = $this->resultPageFactory->create();
    $resultPage->setActiveMenu('Magento_Customer::customer');
    $resultPage->getConfig()->getTitle()->prepend(__('FeedBack'));
    return $resultPage;
  }
}