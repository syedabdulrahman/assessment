<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Controller\Adminhtml\Posts;
/**
 * Feedback admin grid MassDelete controller
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
use Ziffity\Feedback\Controller\Adminhtml\Posts;

class MassDelete extends Posts
{
  /**
    * @return void
    */
  public function execute()
  {
    // Get IDs of the selected news    
    $feedIds = $this->getRequest()->getParam('id');
    $feedIds = explode(',', $feedIds);    
    foreach ($feedIds as $feedId)
    {
      try 
      {
        /** @var $postModel Ziffity\Feedback\Model\Post */
        $postModel = $this->postFactory->create();
        $postModel->load($feedId)->delete();
      } 
      catch (\Exception $e) 
      {
        $this->messageManager->addError($e->getMessage());
      }
    }
    if (count($feedIds)) 
    {
      $this->messageManager->addSuccess(__('A total of %1 record(s) were deleted.', count($feedIds)));
    }
    $this->_redirect('*/*/index');
  }
}