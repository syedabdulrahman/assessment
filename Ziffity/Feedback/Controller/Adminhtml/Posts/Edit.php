<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Controller\Adminhtml\Posts; 
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Ziffity\Feedback\Model\PostFactory;
 
/**
 * Feedback admin grid Edit controller
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
class Edit extends Action
{
  /**
   * @var Magento\Framework\Registry
   */
  protected $coreRegistry;

  /**
   * @var Magento\Framework\View\Result\PageFactory
   */
  protected $resultPageFactory;

  /**
   * @var Ziffity\Feedback\Model\PostFactory
   */
  protected $postFactory;

  /**
   * @param \Magento\Backend\App\Action\Context $context
   * @param Magento\Framework\Registry $coreRegistry
   * @param \Magento\Framework\View\Result\PageFactory $resultPageFactory
   * @param \Ziffity\Feedback\Model\PostFactory $postFactory
   */
  public function __construct(Context $context, Registry $coreRegistry, PageFactory $resultPageFactory, PostFactory $postFactory)
  {
    parent::__construct($context);
    $this->coreRegistry = $coreRegistry;
    $this->resultPageFactory = $resultPageFactory;
    $this->postFactory = $postFactory;
  }

  /**
   * Feedback Edit action
   *
   * @return array $resultPage
   */
  public function execute()
  {
    $resultPage = $this->resultPageFactory->create();
    $resultPage->setActiveMenu('Magento_Customer::customer');
    $resultPage->getConfig()->getTitle()->prepend(__('Edit FeedBack'));
    return $resultPage;    
  }

  /**
   * Feedback access rights checking
   *
   * @return bool
   */
  protected function _isAllowed()
  {
    return true;
  }
}