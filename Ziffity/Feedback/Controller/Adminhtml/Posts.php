<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */ 
namespace Ziffity\Feedback\Controller\Adminhtml;
 
use Magento\Backend\App\Action;
use Magento\Backend\App\Action\Context;
use Magento\Framework\Registry;
use Magento\Framework\View\Result\PageFactory;
use Ziffity\Feedback\Model\PostFactory;

/**
 * Feedback Admin controller
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */
 
class Posts extends Action
{
  /**
   * @var Magento\Framework\Registry
   */
  protected $coreRegistry;

  /**
   * @var Magento\Framework\View\Result\PageFactory
   */
  protected $resultPageFactory;

   /**
   * @var Ziffity\Feedback\Model\PostFactory
   */
  protected $postFactory;

  /**
   * @param \Magento\Backend\App\Action\Context $context
   * @param Magento\Framework\Registry $coreRegistry
   * @param Magento\Framework\View\Result\PageFactory $resultPageFactory
   * @param Ziffity\Feedback\Model\PostFactory $postFactory
   */
  public function __construct(Context $context, Registry $coreRegistry, PageFactory $resultPageFactory, PostFactory $postFactory)
  {
    parent::__construct($context);
    $this->coreRegistry = $coreRegistry;
    $this->resultPageFactory = $resultPageFactory;
    $this->postFactory = $postFactory;
  }
  /**
   * execute action
   *
   * @return null
   */
  public function execute()
  {
    
  }

  /**
   * feedback access rights checking
   *
   * @return bool
   */
  protected function _isAllowed()
  {
    return true;
  }
}