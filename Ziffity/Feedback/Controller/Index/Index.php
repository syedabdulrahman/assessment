<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Controller\Index;
use Magento\Framework\Controller\ResultFactory;
use Ziffity\Feedback\Model\Post;

/**
 * Feedback frontend controller
 *
 * @author      Syed <syed.bakrudin@ziffity.com>
 * @api
 * @since 100.0.2
 * @SuppressWarnings(PHPMD.AllPurposeAction)
 */

class Index extends \Magento\Framework\App\Action\Action
{
    /**
     * @var \Magento\Framework\View\Result\PageFactory
     */
    protected $pageFactory;

    /**
     * @var \Ziffity\Feedback\Model\PostFactory
     */
    protected $_postFactory;

    /**
     * @var Magento\Framework\Controller\ResultFactory
     */
    protected $resultRedirect;

    /**
     * @var \Magento\Framework\Mail\Template\TransportBuilder
     */
    protected $_transportBuilder;

    /**
     * @var \Magento\Framework\Translate\Inline\StateInterface
     */
    protected $inlineTranslation;

    /**
     * @var \Magento\Framework\App\Config\ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var \Magento\Store\Model\StoreManagerInterface
     */
    protected $storeManager;

    /**
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @var \Magento\Framework\Message\ManagerInterface
     */
    protected $messageManager;

    /**
     * Constructor
     * 
     * @param \Magento\Backend\App\Action\Context $context
     * @param \Magento\Framework\View\Result\PageFactory $pageFactory
     * @param \Ziffity\Feedback\Model\PostFactory $_postFactory
     * @param \Magento\Framework\Mail\Template\TransportBuilder $transportBuilder
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Escaper $escaper
     * @param \Magento\Framework\Message\ManagerInterface $messageManager
     */
    public function __construct(\Magento\Framework\App\Action\Context $context, \Magento\Framework\View\Result\PageFactory $pageFactory, \Ziffity\Feedback\Model\PostFactory $_postFactory,\Magento\Framework\Mail\Template\TransportBuilder $transportBuilder, \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation, \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig, \Magento\Store\Model\StoreManagerInterface $storeManager, \Magento\Framework\Escaper $escaper, \Magento\Framework\Message\ManagerInterface $messageManager)
    {
        parent::__construct($context);
        $this->pageFactory = $pageFactory;
        $this->_postFactory = $_postFactory;
        $this->_transportBuilder = $transportBuilder;
        $this->inlineTranslation = $inlineTranslation;
        $this->scopeConfig = $scopeConfig;
        $this->storeManager = $storeManager;
        $this->_escaper = $escaper;
        $this->messageManager = $messageManager;
    }

    /**
     * Feedback action
     *
     * @return string URL
     */
    public function execute()
    {
        $var    = $this->pageFactory->create();
        $post   = (array) $this->getRequest()->getPost();
        $storeEmail = self::getStoreEmail();
        if($post)
        {
            $firstname  = trim($post['customer_fname']);
            $lastname   = trim($post['customer_lname']);
            $email      = trim($post['customer_email']);
            $comments   = trim($post['customer_comment']);
            $fullname   = ucfirst($firstname)." ".ucfirst($lastname);
            $post['fullname'] = $fullname;
            $resultRedirect = $this->resultFactory->create(ResultFactory::TYPE_REDIRECT);
            if(empty($firstname))
            {
                $this->messageManager->addError(__('Please fill the First name.'));
                return $resultRedirect->setUrl("/feedback");
            }elseif(empty($lastname))
            {
                $this->messageManager->addError(__('Please fill the Last name.'));
                return $resultRedirect->setUrl("/feedback");
            }elseif(empty($email))
            {
                $this->messageManager->addError(__('Please fill the Email Id.'));
                return $resultRedirect->setUrl("/feedback");
            }elseif(empty($comments))
            {
                $this->messageManager->addError(__('Please fill the Comment.'));
                return $resultRedirect->setUrl("/feedback");
            }else
            {
                $postObject = new \Magento\Framework\DataObject();
                $postObject->setData($post);
                $sender = [
                    'name' => $this->_escaper->escapeHtml($firstname . $lastname),
                    'email' => $this->_escaper->escapeHtml('syedabdul85@gmail.com'),
                    ];
                $model = $this->_postFactory->create();
                $model->addData([
                    "customer_fname" => $firstname,
                    "customer_lname" => $lastname,
                    "customer_email" => $email,
                    "customer_comment" =>$comments,
                    "status" => 0
                ]);
                $saveData = $model->save();
                if($saveData)
                {
                    $storeScope = \Magento\Store\Model\ScopeInterface::SCOPE_STORE;
                    $transport = $this->_transportBuilder
                    ->setTemplateIdentifier('send_email_email_template')
                    ->setTemplateOptions(['area' => \Magento\Framework\App\Area::AREA_FRONTEND,'store' => \Magento\Store\Model\Store::DEFAULT_STORE_ID])
                    ->setTemplateVars(['data' => $postObject])
                    ->setFrom($sender)
                    ->addTo($email)
                    ->addBcc($storeEmail)
                    ->getTransport();    
                    $transport->sendMessage(); ;
                    $this->inlineTranslation->resume();
                    $this->messageManager->addSuccess(__('Thanks for contacting us with your comments and questions. We\'ll respond to you very soon.'));                    
                    return $resultRedirect->setUrl("/");
                }
            }
        }
        return $var;
    }

    /**
     * Get Store Email ID
     *
     * @return string emailid
     */
    public function getStoreEmail()
    {
        return $this->scopeConfig->getValue(
            'trans_email/ident_general/email',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
    }
}