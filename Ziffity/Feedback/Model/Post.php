<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Model;
/**
 * Feedback Post values.
 */
class Post extends \Magento\Framework\Model\AbstractModel implements \Magento\Framework\DataObject\IdentityInterface
{
	const CACHE_TAG = 'ziffity_feedback_post';
	protected $_cacheTag = 'ziffity_feedback_post';
	protected $_eventPrefix = 'ziffity_feedback_post';

	/**
	 * Define model
	 *
	 * @return void
	 */
	protected function _construct()
	{
		$this->_init('Ziffity\Feedback\Model\ResourceModel\Post');
	}
	public function getIdentities()
	{
		return [self::CACHE_TAG . '_' . $this->getId()];
	}
	public function getDefaultValues()
	{
		$values = [];
		return $values;
	}
}
