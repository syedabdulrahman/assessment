<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Ziffity\Feedback\Model\ResourceModel;
/**
 * Feedback table resource model block.
 */
class Post extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb
{	
	public function __construct(\Magento\Framework\Model\ResourceModel\Db\Context $context)
	{
		parent::__construct($context);
	}
	
	protected function _construct()
	{
		$this->_init('ziffity_feedback_post', 'id');
	}
}