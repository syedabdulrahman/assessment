<?php
/**
 * @category Custome_Module
 * @package  Feedback
 * @author   syed <syed.bakrudin@ziffity.com>
 * @license  syed http://local.site1.com/feedback
 * @link     http://local.site1.com/feedback
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */

use \Magento\Framework\Component\ComponentRegistrar;
ComponentRegistrar::register(ComponentRegistrar::MODULE, 'Ziffity_Feedback', __DIR__);
