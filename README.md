# Assessment

# Customer Feedback Module

## How to install & upgrade Ziffity_Feedback

### 1. Install via composer (recommend)

We recommend you to install Ziffity_Feedback module via composer. It is easy to install, update and maintaince.

Run the following command in Magento 2 root folder.

#### 1.1 Install

```
composer update Ziffity/Feedback
php bin/magento module:status
php bin/magento module:enable Ziffity_Feedback
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy
```

#### 1.2 Upgrade

```
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy
```

Run compile if your store in Product mode:

```
php bin/magento setup:di:compile
```

### 2. Copy and paste

If you don't want to install via composer, you can use this way. 

- Download [the latest version here](git clone https://bitbucket.org/syedabdulrahman/assessment.git)
- Clone the repo and enable the module through terminal.
- Place the folder inside the app/code/<module>
- Go to Magento root folder and run upgrade command line to install `Ziffity_Feedback`:

```
php bin/magento module:status
php bin/magento module:enable Ziffity_Feedback
php bin/magento setup:upgrade
php bin/magento setup:static-content:deploy
```

#Clear the Cache

